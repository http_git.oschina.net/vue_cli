开发工具：
	Node: npm管理包
	Nginx： 代理解决跨域、负载均衡
	工具： 任意文本编辑器 sublime vscode

nginx配置：
nginx在svn中tools文件夹;
nginx配置将conf/nginx.conf中
      location / {
	   root F:/java_workspace/vue_cli/dist; //项目的根目录
	   index index.html;
      }

运行步骤
1. 安装node(安装包在svn中tools文件夹)（注： 首次需要）
cmd打开终端 npm install -g cnpm --registry=https://registry.npm.taobao.org //淘宝映像，速度快

2. 打开到mall-pro，目录
3. cnpm install //安装依赖包 （注： 首次需要）
4. npm run dev //编译项目 编译到dist文件
5. 启动nginx.exe
6. 输入网址 http://10.108.66.103:1121

文件目录：
common 	--公共方法
components --  公共组件
dist	--编译后文件
doc		--文档存储
node_modules	--管理包
src		--开发代码

5、运行
（1）打开nginx
（2）终端执行，npm run dev 执行项目
（3）浏览器打开： http://本机ip：nginx端口号
（4）图片img文件夹，放到nginx目录中html文件夹，（部署至服务器也如此）

6.
现在流行的就是fetch和axios，axios封装完全，开包即用,但fetch需要自己封装

7.参考的项目地址：
   https://github.com/wclimb/vue-video

8.vue_cli项目
  a.main.js(webpack.base.conf.js配置)--->引入各个组件
  b.通过 body-parser(dev-server.js配置) 读取 db.json 静态数据文件
  
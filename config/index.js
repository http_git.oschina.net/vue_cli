// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')

module.exports = {
  build: {
    env: require("./prod.env"),
    index: path.resolve(__dirname, "../dist/index.html"),
    assetsRoot: path.resolve(__dirname, "../dist"),
    assetsSubDirectory: "static",
    assetsPublicPath: "./",//错误原因 Opening index.html over file:// won't work.
    productionSourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    devtool: "#cheap-module-source-map",
    productionGzipExtensions: ["js", "css"]
  },
  dev: {
    env: require("./dev.env"),
    //host: "10.108.66.103", // can be overwritten by process.env.HOST
    port: 8080,
    autoOpenBrowser: false,
    assetsSubDirectory: "static",
    assetsPublicPath: "/",
    proxyTable: {
      "/api": {
        target: "http://localhost:8081/api", //apiServer 挂的是 /api，Rewrite把 /api 重写成 / 了, 所以你的 8081 接收到的请求 是不会带 /api 的..
        changeOrigin:true,//设置这个参数可以避免跨越
        pathRewrite:{ 
          '^/api':''
        }
      } 
     /**
      * 你前端请求应该是localhost:8080/api/api/getNewList
      * localhost:8080/api，这个是代理
      * api/getNewList，这个是请求
        localhost:8080/api这个代理到localhost:8081
        后端接口带的api和代理的api属于两个东西
      */
    },
    poll: false,
    useEslint: true,
    showEslintErrorsInOverlay: false,
    devtool: "eval-source-map",//开发环境设置，有利于bugger调试
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cacheBusting: true,
    cssSourceMap: false
  }
};

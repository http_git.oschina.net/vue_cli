//外部点击的事件传递组件内部，从而实现组件之间的交互
import Vue from 'vue'
const eventBus = new Vue();

export  {eventBus} //变量的导出，必须使用{} ,不要使用export default
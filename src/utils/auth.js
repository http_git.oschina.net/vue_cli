/**
 * 存储localStorage
 */
export const setToken = (name,content) => {
    if(!name) null;
    if(typeof content !== "string"){
        content = JSON.stringify(content);
    }
    window.localStorage.setItem(name,content); 
}

/**
 * 获取localStorage
 */

 export const getToken = (name) =>{
     if(!name) null;
     return window.localStorage.getItem(name);
 }

 /**
  * 删除localStorage
  */
export const removeToken = (name) => {
    if(!name) null;
    return window.localStorage.removeItem(name);
}


import axios from 'axios'
import qs from 'qs'
import env from './env'

//添加一个请求拦截器
axios.interceptors.request.use(config => {
    //loading
    return config
},error =>{
   return Promise.reject(error) //此处使用promise.reject()
})

//添加一个响应拦截器
axios.interceptors.response.use(response => { //必须使用response而不是config
    const res = response.data;
    console.log('response拦截  '+ JSON.stringify(res));
    if(!res.code && res.code === 0){
        return response.data;
    }else if(res.code === 406){
        //tokenId失效,重新登录
        //删除信息，调到登录

        return Promise.reject('error');
    }else{
        
        return Promise.reject('error');
    }
},error =>{
    if (error && error.response) {
        switch (error.response.status) {
            case 400:
                error.message = '请求错误(400)';
                break;
            case 401:
                error.message = '未授权，请重新登录(401)';
                break;
            case 403:
                error.message = '拒绝访问(403)';
                break;
            case 404:
                error.message = '请求出错(404)';
                break;
            case 408:
                error.message = '请求超时(408)';
                break;
            case 500:
                error.message = '服务器错误(500)';
                break;
            case 501:
                error.message = '服务未实现(501)';
                break;
            case 502:
                error.message = '网络错误(502)';
                break;
            case 503:
                error.message = '服务不可用(503)';
                break;
            case 504:
                error.message = '网络超时(504)';
                break;
            case 505:
                error.message = 'HTTP版本不受支持(505)';
                break;
            default:
                error.message = `连接出错(${error.response.status})!`;
        }
    } else {
        error.message = '连接服务器失败!'
    }
    return Promise.reject(error.response) //此处使用promise.reject()
})

// 暂时不需要
function checkStatus(response){
    //如果http状态码正常，则直接返回数据
    if(response && (response.status === 200 || response.status === 304 || response.status === 400)){
        return response
        //如果不需要除了data之外的数据，可以直接return response.data
    }

    //异常状态下，把错误信息返回去
    return{
        status:-404,
        msg:'网络异常'
    }
}

// 暂时不需要
function checkCode(res){
   //如果code异常(这里已经包括网络异常，服务器错误，后端抛出的错误) ，可以弹出一个错误提示，告诉用户
   debugger;
   if(res.status === -404){
       alert (res.msg)
   }

   if(res.data && (!res.data.success)){
       alert(res.data.error_msg);
   }
   return res;
}

//另外一种方式 module.exports = { }
export default {
  //post请求
  post(url,data){
    console.log('env.baseUrl  '+env.baseUrl);
    return axios({
        method:'post',
        baseURL:env.baseUrl ,//全局修改axios默认配置 ,如果url不是绝对路径，那么会将baseURL和url拼接作为请求的接口地址,用来区分不同环境，建议使用
        url,// `url` 是请求的接口地址
        data:qs.stringify(data), //请求体数据,只有当请求方法为'PUT', 'POST',和'PATCH'时可用
        timeout:10000,
        withCredentials: false, // default ,是否携带cookie信息
        headers:{
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    }).then( (response) =>{
        //检查状态
        return   checkStatus(response);
       // console.log(response);
    }).then( (res) => {
        //检查状态码
       //console.log(res);
       return checkCode(res);
    })
  },

  //get请求
  get(url,data){
   return axios({
       method:'get',
       baseURL:env.baseUrl,
       url,
       params:data,  //URL参数,必须是一个纯对象或者 URL参数对象
       timeout:10000,
       headers:{ 'X-Requested-With': 'XMLHttpRequest'}
   }).then( (response) =>{
       //检查状态
      return checkStatus(response);
   }).then( (res) =>{
       //检查状态码
      return checkCode(res);
   })
  }
}

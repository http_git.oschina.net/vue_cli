import axios from 'axios'
import {Message, MessageBox} from 'element-ui'
import store from '../store'
import {getToken, setToken, removeToken} from '@/utils/auth'
import router from "../router";

const overtime = 15000
// 创建axios实例
const service = axios.create({
    baseURL: process.env.NODE_ENV === 'production' ? process.env.BASE_API : '/api', // api的base_url
    timeout: overtime // 请求超时时间
})

//默认 正确响应200-300 添加304校验
service.defaults.validateStatus = function(status) {
    return (status >= 200 && status < 300) || status === 304;
}

// request拦截器
service.interceptors.request.use(config => {
    console.log('request拦截器的请求config.url:',config.url,'  请求config.baseURL:',config.baseURL);
    console.log('请求过滤config.url中includes(): ',config.url.includes('/login'),'config.url中indexOf()方法: ' ,config.url.indexOf('/login') >-1);
    
    //获取用户信息
    if (store.getters.token && store.getters.token.length > 0) {
        config.headers['loginToken'] = store.getters.token // 让每个请求携带自定义token 请根据实际情况自行修改
        config.headers['accreditCode'] = store.getters.msg  //授权码
    }
    return config;
}, error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(response => {
        console.log(response.status && response.status === 200);
        // 模拟mockjs返回数据
        if (response.status && response.status === 200) {
            console.log(response.data);
            return response.data;
        } else if (response.status === 406) {
            removeToken('loginToken');
            //store.dispatch('setInfo',null);
            store.dispatch('setAuth',false);
            router.push({name: 'login'});  //跳转登录
            return Promise.reject('error')
        } else {
            Message({
                message: response.statusText,
                type: 'error',
                duration: 5 * 1000
            })
            // store.dispatch('SetMsg', '')
            return Promise.reject('error')
        }

        //const res = response.data;
        // if (!res.code || res.code === 1) {
        //     return response.data
        // } else if (res.code === 3) {
        //     removeToken('loginToken');
        //     //store.dispatch('setInfo',null);
        //     store.dispatch('setAuth',false);
        //     router.push({name: 'login'});  //跳转登录
        //     return Promise.reject('error')
        // } else {
        //     Message({
        //         message: res.msg,
        //         type: 'error',
        //         duration: 5 * 1000
        //     })
        //     // store.dispatch('SetMsg', '')
        //     return Promise.reject('error')
        // }
    }, err => {
        //store.dispatch('hideloader');
        console.log('err报错信息',err.response);  // 输出的结果: err报错信息 ReferenceError: url is not defined
        if (err && err.response) {
            switch (err.response.status) {
                case 400:
                    err.message = '请求错误(400)';
                    break;
                case 401:
                    err.message = '未授权，请重新登录(401)';
                    break;
                case 403:
                    err.message = '拒绝访问(403)';
                    break;
                case 404:
                    err.message = '请求出错(404)';
                    break;
                case 408:
                    err.message = '请求超时(408)';
                    break;
                case 500:
                    err.message = '服务器错误(500)';
                    break;
                case 501:
                    err.message = '服务未实现(501)';
                    break;
                case 502:
                    err.message = '网络错误(502)';
                    break;
                case 503:
                    err.message = '服务不可用(503)';
                    break;
                case 504:
                    err.message = '网络超时(504)';
                    break;
                case 505:
                    err.message = 'HTTP版本不受支持(505)';
                    break;
                default:
                    err.message = `连接出错(${err.response.status})!`;
            }
        } else {
            err.message = '连接服务器失败!'
        }
        Message({
            message: err.message,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(err)
    }
)

export default service
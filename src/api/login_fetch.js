import fetch from '../utils/fetch'

/**
 * 登录请求
 */
export const login = data => fetch('/login',data,'post') 

export const loginGet = data => fetch('/login',data,'get') 
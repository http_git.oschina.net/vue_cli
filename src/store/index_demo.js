import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

//初始化store实例
export default new Vuex.Store({//注意是大写而不是小写
  //数据流
  state: {
    totalPrice: 0
  },
  getters: {
    getTotal(state) {
      //通过方法返回计算结果
      return state.totalPrice;
    }
  },
  mutations: {
    increment(state, price) {
      state.totalPrice += price;
    },
    decrement(state, price) {
      state.totalPrice -= price;
    }
  },
  actions: {
    //通过actions方法实现相同的功能,此次actions作为中介
    increment(context, prie) {
      context.commit("increment", prie); //此次传递到helloworld的addOne方法中，使用dispatch
    }
  }
});

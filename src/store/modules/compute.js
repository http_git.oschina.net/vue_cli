
const state = {
   //存储数据
   show:true,
   name:'彭忠艳'
}

const getters = {
    //获取数据
    show: state => !state.show
}

// mutation是响应式的，导致刷新页面后仓库被清空了,恢复到初始化状态
// state的值刷新后会没了，而localStorage的值不能响应式地变化（Vue 仅可以对其管理的数据做响应式处理，可以理解为 data 中的数据，localStorage 并不在 Vue 的管理下，自然不会有响应特性
const mutations = {
  switch_mut: (state) => {
    console.log('使用mutations改变show的状态 , 通过提交commit mutations改变sotre状态,即现在的状态为：' + state.show);//这里的state对应着上面这个state
    state.show = state.show ? false : true;
    // 你还可以在这里执行其他的操作改变state
    sessionStorage.setItem('change',JSON.stringify(state.show));
  },
  getStorage_mut: (state) => {
    //更改state的状态值需要在mutations中更改
    if(sessionStorage.getItem('change')){
      state.show = JSON.parse(sessionStorage.getItem('change'));
    }
  }
}

const actions = {
    switch_act: (context) => {//这里的context和我们使用的$store拥有相同的对象和方法
        console.log('使用actions改变show的状态,在组件使用this.$store.dispatch()分发actions');
        console.log('actions再通过context.commit()提交一个mutations,而不是直接变更状态');
        context.commit('switch_mut'); 
        //你还可以在这里触发其他的mutations方法
    },
    getStorage_act: (context) => {
      context.commit('getStorage_mut'); 
    }
}

//构建数据中心
export default {
  state,
  getters,
  actions,
  mutations
}
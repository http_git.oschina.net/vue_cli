import Vue from 'vue'

const state = {
    //存储数据
    orderList : [],
    params:{}
}

const getters = {
    //从页面获取存储的数据
    orderList : state => state.orderList
}


const actions = {
    //异步的操作,或者通过api获取
    //commit用来调用mutations,state用来获取存储的数据
    fetchOrderList({commit,state}){
        console.log('fetchOrderList',state);
        Vue.http.post('/api/getOrderList',state.params).then((res) =>{
            console.log('使用store管理数据，获取data' ,res.data.list);
           commit('changeOrderList',res.data.list);
        },(err) =>{

        })
    }
}

const mutations = {
   //同步的操作,参数orderList从页面或者actions中传递过来
   changeOrderList(state,payOrderList){
       state.orderList = payOrderList
   },
   updateParams(state,{key,value}){  //updateParams(state,params)  同步刷新
      console.log('updateParams的state',state);
      console.log('updateParams的key & vaule ',{key,value});
      state.params[key] = value //简化模式处理 state.params[params.key] = params.value;
   }
}

//构建数据中心
export default  {
  state,
  getters,
  actions,
  mutations
}
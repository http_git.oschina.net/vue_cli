import vuex from 'Vuex'
import vue from 'vue'
import orderList from './modules/orderList'
import compute from './modules/compute' //使用count.vue文件中使用
import Vue from 'vue'

vue.use(vuex)

export default new vuex.Store({
  modules: {
    orderList,
    compute
  },
  strict:process.env.NODE_ENV !== 'production'
});
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// import App from './App' //使用App组件,作为主页面
import layout from './components/layout' //使用布局组件，作为主页面
import router from './router'
import resource from 'vue-resource'
import store from './store'
import echarts from 'echarts';

require('./mock/mock.js')

Vue.use(resource) //使用的$http.post()请求
Vue.prototype.$echarts = echarts;
Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,//注入全局 挂载store
  router,//注入全局
  components: { layout },
  template: "<layout/>"
});

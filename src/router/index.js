import Vue from 'vue'
import Router from 'vue-router'
import {routers} from  './router'

Vue.use(Router);

export default new Router({
  mode:'history',//访问路径不带#号
  routes:routers,//数组结构
  strict:process.env.NODE_ENV !== 'production'
})
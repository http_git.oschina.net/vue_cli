import detail from '@/views/detail'

// 作为整个页面的路由
export const pageRouter = [{
    path: '/',
    name:'首页',
    redirect: '/index' //重定向处理
  },{
    path:'/index',
    name:'首页',
    component:resolve => {require(['@/views/index'],resolve)} //@相等于..
 },{ 
     path:'/orderList',
     name:'订单列表',
     component:resolve => {require(['@/views/orderList'],resolve)} //引入组件还有一种懒加载的方式。懒加载引入的优点是：当你访问这个页面的时候才会去加载相关的资源，这样的话能提高页面的访问速度
  }]

export const appRouter = {
    path:'/detail',
    component:detail,
    redirect:'/detail/analysis',//默认访问第一个页面，刚进入应用就渲染某个路由组件
    children:[
      {
       path:'analysis',//注意子目录前缀没有/
       name:'数据预测',
       component:resolve => {require(['@/views/detail/analysis'],resolve)} 
      },{
       path:'count',
       name:'数据统计',
       component:resolve => {require(['@/views/detail/count'],resolve)}
      },{
       path:'forecast',
       name:'流量分析',
       component:resolve => {require(['@/views/detail/forecast'],resolve)}
      },{
       path:'publish',
       name:'广告发布',
       component:resolve => {require(['@/views/detail/publish'],resolve)}
      }  
    ]
}

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
	...pageRouter,//数组
    appRouter //对象
];

/** 
 * 合并对象
 * let arr1 = [0, 1, 2];
 * let arr2 = [...arr1, 3]; // arr2 = [0, 1, 2, 3]
*/

import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import index from '../views/index'
import detail from '../views/detail'
import Analysis from '../views/detail/analysis'
import count from '../views/detail/count'
import forecast from '../views/detail/forecast'
import publish from '../views/detail/publish'
import orderList from '../views/orderList'

Vue.use(Router)

// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'HelloWorld',
//       component: HelloWorld
//     }
//   ]
// })

export default new Router({
  mode:'history',
  routes:[
    {
      path: '/',
      name:'首页',
      redirect: '/index'
    },{
      path:'/index',
      name:'首页',
      component:index
   },
    { 
       path:'/orderList',
       name:'订单列表',
       component:orderList
    },
    {
       path:'/detail',
       component:detail,
       redirect:'/detail/analysis',//默认访问第一个页面，刚进入应用就渲染某个路由组件
       children:[
         {
          path:'analysis',//注意子目录前缀没有/
          name:'数据预测',
          component:Analysis
         },{
          path:'count',
          name:'数据统计',
          component:count
         },{
          path:'forecast',
          name:'流量分析',
          component:forecast
         },{
          path:'publish',
          name:'广告发布',
          component:publish
         }  
       ]
    }
]
})
